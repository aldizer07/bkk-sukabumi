<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bursa Kerja Khusus | Login</title>
    {{-- Bootstrap 4.4.1 --}}
    <link rel="stylesheet" href="{{ asset('bootstrap-4.4.1/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
    <style>
        body {
            background-color: blueviolet;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .box {
            margin: 50px 20px 0 20px;
            background-color: #ffffff;
            border-radius: 10px;
            padding: 30px;
        }
        @media(max-width: 992px) {
            #garis {
                border-bottom: 1px solid blueviolet;
            }
        }
        h3 {
            color: blueviolet;
            margin-top: 20px;
        }
        @media(min-width: 993px) {
            #garis {
                border-right: 1px solid blueviolet;
            }
        }
        .form-control {
            border-radius: 0;
        }
        label {
            color: #8e8e8e;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="box">
            <div class="row">
                <div class="col-md-12 col-lg-4" id="garis">
                    <h3>Login</h3>
                    <hr>
                    @if (Session::has('message'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
                    <form action="{{ route('loginprocess.admin') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <button class="btn btn-warning">Login</button>
                    </form>
                    <br>
                </div>
                <div class="col-md-12 col-lg-8 text-center">
                    <h3 class="mb-4">Selamat datang di Bursa Kerja Khusus <br> SMK Negeri 1 Sukabumi</h3>
                    <a href="{{ url('/') }}" class="btn btn-sm btn-success">Halaman Utama</a>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap-4.4.1/js/bootstrap.min.js') }}"></script>
</body>
</html>