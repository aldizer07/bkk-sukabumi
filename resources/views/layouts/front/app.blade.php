<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    {{-- Bootstrap 4.4.1 --}}
    <link rel="stylesheet" href="{{ asset('bootstrap-4.4.1/css/bootstrap.min.css') }}">
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
    <style>
        html, body {
            background-color:white;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            margin:0;
            padding:0;
            height:100%;
        }
        .jumbotron {
            background-image: url('{{ asset("images/bg.jpg") }}');
            background-size: cover;
            height: 650px;
        }
        #footer {
            height: 100px; padding:20px; color:white; bottom:0; position: relative;;
        }
    </style>
    @yield('css')
</head>
<body>
    @include('layouts.front.modules.navbar')

    @yield('content')

    @include('layouts.front.modules.footer')
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('bootstrap-4.4.1/js/bootstrap.min.js') }}"></script>
    @yield('js')
</body>
</html>