@extends('layouts.admin.master')
@section('title','Tambah Admin Sekolah')
@section('css')
<link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
@stop
@section('content') 
    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('alumni.index') }}" class="btn btn-circle"><i class="fa fa-arrow-left"></i></a> Data Admin</h6>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
                    <form action="{{ route('admin.store') }}" method="post">
                        @csrf
                        <div class="form-group row">
                           <div class="col-md-6 mb-3">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}">
                                @error('name')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                           </div>
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}">
                                @error('email')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="email">No. Tlp</label>
                                <input type="text" class="form-control form-control-sm @error('phonenumber') is-invalid @enderror" name="phonenumber" value="{{ old('phonenumber') }}">
                                @error('phonenumber')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" name="password" value="{{ old('passowrd') }}">
                                @error('passowrd')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="sekolah">Sekolah</label>
                                <select name="school_id" id="school_name" class="form-control form-control-sm @error('school_id') is-invalid @enderror ">
                                    <option value="">Pilih Sekolah</option>
                                    @foreach ($school as $item)
                                        <option value="{{ $item->id }}">{{ $item->sekolah }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-sm">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{ asset('select2/js/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#school_name').select2({
            width: 'resolve'
        });
    });
</script>
@stop