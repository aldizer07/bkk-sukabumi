@extends('layouts.admin.master')
@section('title','Daftar Admin Sekolah')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Admin Sekolah</h6>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <a href="{{ route('admin.create') }}" class="btn btn-success btn-sm">Tambah Data</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th width="180">Nama</th>
                        <th>Email</th>
                        <th>No Telp.</th>
                        <th>Sekolah</th>
                        <th>Alamat</th>
                        <th width="100">#</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($admin as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->phonenumber }}</td>
                            <td>{{ $item->sekolah->sekolah }}</td>
                            <td>{{ $item->sekolah->alamat_jalan }}, {{ $item->sekolah->kecamatan }}, {{ $item->sekolah->kabupaten_kota }}, {{ $item->sekolah->propinsi }}</td>
                            <td>
                                <a href="{{ route('admin.edit', $item->id) }}" class="btn btn-warning btn-sm btn-circle"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('admin.destroy', $item->id) }}" class="btn btn-danger btn-sm hapus btn-circle"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Tidak ada data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {!! $admin->links() !!}
    </div>
</div>
@endsection
@section('js')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.hapus').on('click', function(){
                swal({
                    title: "Apa anda yakin?",
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete) {
                        $.ajax({
                            url: $(this).attr('href'),
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success:function(){
                                swal("Data berhasil dihapus", {
                                    icon: "success",
                                }).then((willDelete) => {
                                    window.location="{{ route('admin.index') }}"
                                });
                            }
                        });
                    } else {
                        swal("Data aman");
                    }
                });

                return false;
            });
        }); 
    </script>
@endsection