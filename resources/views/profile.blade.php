@extends('layouts.front.app')
@section('title', $profile->name)
@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"></h6>
            </div>
            <div class="card-body">
                @if (Session::has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>                        
                @endif
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{ asset('storage/photo/' . (empty($profile->photo) ? 'default.png' : $profile->photo)) }}" width="200" height="200" class="image-circle mb-3">
                    </div>
                    <div class="col-md-8">
                        <form action="{{ route('jobseeker.edit',auth()->guard('students')->user()->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{-- @method('put') --}}
                            <div class="form-group">
                                <input type="text" name="nisn" class="form-control" value="{{ auth()->guard('students')->user()->nisn }}" readonly>
                            </div>
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" value="{{ auth()->guard('students')->user()->name }}" readonly>
                            </div>
                            <div class="form-group">
                                @if (auth()->guard('students')->user()->gender != null)
                                <input type="text" name="gender" class="form-control" value="{{ auth()->guard('students')->user()->gender }}" readonly>
                                @else
                                    <select name="gender" id="" class="form-control" required>
                                        <option >Laki-laki</option>
                                        <option>Perempuan</option>
                                    </select>
                                    <small class="text-muted">Merubah Jenis Kelamin hanya dapat dilakukan sekali, mohon isi dengan benar</small>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="text" name="place_of_birth" class="form-control" value="{{ auth()->guard('students')->user()->place_of_birth }}" placeholder="Tempat lahir">
                            </div>
                            <div class="form-group">
                                <input type="date" name="date_of_birth" class="form-control" value="{{ auth()->guard('students')->user()->date_of_birth }}">
                            </div>
                            <div class="form-group">
                                <input type="text" name="address" class="form-control" value="{{ auth()->guard('students')->user()->address }}" placeholder="Alamat">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" value="{{ auth()->guard('students')->user()->email }}">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="password">
                                <small class="text-muted">Kosongkan jika tidak ingin merubah password</small>
                            </div>
                            <div class="form-group">
                                <input type="file" name="foto" class="form-control">
                                <small class="text-muted">Kosongkan jika tidak ingin merubah foto</small>
                            </div>
                            <div class="form-group">
                                <input type="file" name="cv" class="form-control">
                                <small class="text-muted">Kosongkan jika tidak ingin merubah cv</small>
                            </div>
                            <button class="btn btn-primary btn-sm">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection