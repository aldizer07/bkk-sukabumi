@extends('layouts.front.app')
@section('title','Halaman Login - Register')
@section('css')
<link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
<style>
    .box {
        margin: 0 20px 0 20px;
        background-color: #ffffff;
        border-radius: 10px;
        padding: 30px;
    }
    .border {
        border: 1px solid #ededed; padding:30px; 
    }
    .form-control {
        border-radius: 0;
    }
    label {
        color: #8e8e8e;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="box">
        @if (Session::has('message'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('message') }}
                @php 
                    Session::forget('message')
                @endphp
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>                        
        @endif
        @if (Session::has('error'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ Session::get('error') }}
                @php 
                    Session::forget('error')
                @endphp
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>                        
        @endif
        <div class="row">
            <div class="col-md-12 col-lg-4 border">
                <h3>Login</h3>
                <hr>
                <form action="{{ route('login.process') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" name="nisn" id="nisn" class="form-control @error('nisn_login') is-invalid @enderror" value="{{ old('nisn_login') }}">
                        @error('nisn_login')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nisn">Password</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button class="btn btn-primary">Login</button>
                </form>
                <br>
            </div>
            <div class="col-md-12 col-lg-8 border">
                <h3>Daftar</h3>
                <hr>
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="nisn">Nama Lengkap</label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="nisn_register">NISN</label>
                            <input type="text" name="nisn_register" id="nisn_register" class="form-control @error('nisn_register') is-invalid @enderror" value="{{ old('nisn_register') }}">
                            @error('nisn')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="date_of_birth">Tanggal Lahir</label>
                            <input type="date" name="date_of_birth" id="date_of_birth" class="form-control @error('date_of_birth') is-invalid @enderror" value="{{ old('date_of_birth') }}">
                            @error('date_of_birth')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="school_name">Nama Sekolah</label>
                            <select name="npsn" id="school_name" class="form-control" required style="width: 100%">
                                <option value="">Pilih Sekolah</option>
                                @foreach ($sekolah as $item)
                                    <option value="{{ $item->id }}">{{ $item->sekolah }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="password">Password</label>
                            <input type="password" name="password_register" id="password" class="form-control @error('password_register') is-invalid @enderror">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> 
                    </div>
                    <button class="btn btn-primary">Daftar</button>
                </form>
                <hr>
                <p class="text-danger">Gunakan data anda dengan benar agar bisa diproses lamarannya!</p>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('select2/js/select2.min.js') }}"></script>

<script>
    $(document).ready(function(){

        $('#school_id').select2();
    });
</script>
@endsection