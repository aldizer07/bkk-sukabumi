@extends('layouts.admin.master')
@section('title',$student->name)
@section('css')
<link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
@stop
@section('content') 
    <div class="row">
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('alumni.index') }}" class="btn btn-circle"><i class="fa fa-arrow-left"></i></a> Data Pribadi</h6>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
                    <form action="{{ route('alumni.update',$student->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                           <div class="col-md-6 mb-3">
                                <label for="nisn">NISN</label>
                                <input type="text" class="form-control form-control-sm @error('nisn') is-invalid @enderror" name="nisn" value="{{ $student->nisn }}">
                                @error('nisn')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                           </div>
                           <div class="col-md-6 mb-3">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ $student->name }}">
                                @error('name')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                           </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <label for="gender">Jenis Kelamin</label>
                                <select name="gender" id="gender" class="form-control form-control-sm @error('gender') is-invalid @enderror">
                                    <option {{ $student->gender == 'Laki-laki' ? 'selected' : '' }}>Laki-laki</option>
                                    <option {{ $student->gender == 'Perempuan' ? 'selected' : '' }}>Perempuan</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ $student->email }}">
                                @error('email')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 mb-3">
                                <label for="place_of_birth">Tempat Lahir</label>
                                <input type="text" class="form-control form-control-sm @error('place_of_birth') is-invalid @enderror" name="place_of_birth" value="{{ $student->place_of_birth }}">
                                @error('place_of_birth')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="date_of_birth">Tanggal Lahir</label>
                                <input type="date" class="form-control form-control-sm @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ $student->date_of_birth }}">
                                @error('date_of_birth')
                                    <div class="invalid-feedback" role="alert">
                                        <span>{{ $message }}</span>
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat</label>
                            <textarea name="address" id="" cols="5" rows="5" class="form-control form-control-sm @error('address') is-invalid @enderror">{{ $student->address }}</textarea>
                            @error('address')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <input type="hidden" name="npsn" value="{{ $student->school_id }}"> 
                        <hr>
                        <button class="btn btn-primary">Simpan</button>    
                </div>
            </div>
        </div>
        @if (auth()->user()->level == 0)
        <div class="col-md-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Data Sekolah</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="school_name">Nama Sekolah</label>
                        <select name="npsn" id="school_name" class="form-control" required style="width: 100%">
                            <option value="">Pilih Sekolah</option>
                            
                                @foreach ($sekolah as $item)
                                <option value="{{ $item->id }}" {{ $student->school_id == $item->id ? 'selected' : '' }}>{{ $item->sekolah }}</option>
                            @endforeach
                        </select>
                    </div>                  
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Upload Form</h6>
                </div>
                <div class="card-body">
                        <div class="form-group">
                            <label for="photo">Foto</label>
                            <input type="file" name="photo" class="form-control @error('photo') is-invalid @enderror">
                            @error('photo')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="cv">CV</label>
                            <input type="file" name="cv" class="form-control @error('cv') is-invalid @enderror">
                            @error('cv')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div><hr>
                        <button class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
@section('js')
<script src="{{ asset('select2/js/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#school_name').select2({
            width: 'resolve'
        });
    });
</script>
@stop