@extends('layouts.admin.master')
@section('title','Daftar Alumni')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Daftar Alumni</h6>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <a href="{{ route('alumni.create') }}" class="btn btn-success btn-sm">Tambah Data</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        {{-- <th>Alamat</th> --}}
                        <th>Tempat, Tanggal Lahir</th>
                        <th>Sekolah</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($students as $student)
                        <tr>
                            <td>{{ $student->nisn }}</td>
                            <td>{{ $student->name }}</td>
                            <td>{{ $student->gender }}</td>
                            {{-- <td>{{ $student->address }}</td> --}}
                            <td>{{ $student->place_of_birth }}, {{ date('d-m-Y',strtotime($student->date_of_birth)) }}</td>
                                <td>{{ $student->sekolah->sekolah }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-sm btn-circle" data-toggle="modal" data-target="#show{{ $student->id }}">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <a href="{{ route('alumni.edit', $student->id) }}" class="btn btn-warning btn-sm btn-circle"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('alumni.destroy', $student->id) }}" class="btn btn-danger btn-sm hapus btn-circle"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">Tidak ada data</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        {!! $students->links() !!}
    </div>
    <!-- Modal -->
    @foreach ($students as $item)
    <div class="modal fade" id="show{{ $item->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">{{ $item->name }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                        <img src="{{ asset('storage/photo/'.$item->photo) }}" alt="{{ $item->name }}" width="100" height="100">
                    </div>
                    <div class="col-md-9 col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>NISN</th>
                                    <td>:</td>
                                    <td>{{ $item->nisn }}</td>
                                </tr>
                                <tr>
                                    <th>Nama Alumni</th>
                                    <td>:</td>
                                    <td>{{ $item->name }}</td>
                                </tr>
                                <tr>
                                    <th>Jenis Kelamin</th>
                                    <td>:</td>
                                    <td>{{ $item->gender }}</td>
                                </tr>
                                <tr>
                                    <th>Tempat, Tanggal Lahir</th>
                                    <td>:</td>
                                    <td>{{ $item->place_of_birth }}, {{ $item->date_of_birth }}</td>
                                </tr>
                                <tr>
                                    <th>Alamat Alumni</th>
                                    <td>:</td>
                                    <td>{{ $item->address }}</td>
                                </tr>
                                <tr>
                                    <th>Email Alumni</th>
                                    <td>:</td>
                                    <td>{{ $item->email }}</td>
                                </tr>
                                <tr>
                                    <th>Alumni Sekolah</th>
                                    <td>:</td>
                                    <td>{{ $item->sekolah->sekolah }}</td>
                                </tr>
                                <tr>
                                    <th>Curiculum Vitae</th>
                                    <td>:</td>
                                    <td>
                                        @if (empty($item->cv))
                                            -
                                        @else
                                        <a href="{{ asset('storage/cv/'.$item->cv) }}" target="blank">Lihat disini</a>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
            </div>
        </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
@section('js')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.hapus').on('click', function(){
                swal({
                    title: "Apa anda yakin?",
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete) {
                        $.ajax({
                            url: $(this).attr('href'),
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success:function(){
                                swal("Data berhasil dihapus", {
                                    icon: "success",
                                }).then((willDelete) => {
                                    window.location="{{ route('alumni.index') }}"
                                });
                            }
                        });
                    } else {
                        swal("Data aman");
                    }
                });

                return false;
            });
        }); 
    </script>
@endsection