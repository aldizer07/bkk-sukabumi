@extends('layouts.admin.master')
@section('title','Profile - '.auth()->user()->name)
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{ auth()->user()->name }}</h6>
    </div>
    <div class="card-body">
        @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
        <form action="{{ route('profile.update', auth()->user()->id) }}" method="post">
            @csrf
            @method('put')
            <div class="form-group row">
               <div class="col-md-6 mb-3">
                    <label for="name">Nama</label>
                    <input type="text" class="form-control form-control-sm @error('name') is-invalid @enderror" name="name" value="{{ auth()->user()->name }}">
                    @error('name')
                        <div class="invalid-feedback" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
               </div>
                <div class="col-md-6">
                    <label for="email">Email</label>
                    <input type="text" class="form-control form-control-sm @error('email') is-invalid @enderror" name="email" value="{{ auth()->user()->email }}">
                    @error('email')
                        <div class="invalid-feedback" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <label for="email">No. Tlp</label>
                    <input type="text" class="form-control form-control-sm @error('phonenumber') is-invalid @enderror" name="phonenumber" value="{{ auth()->user()->phonenumber }}">
                    @error('phonenumber')
                        <div class="invalid-feedback" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="password">Password</label>
                    <input type="password" class="form-control form-control-sm @error('password') is-invalid @enderror" name="password" }}>
                    <small class="text-muted">Kosongkan bila tidak ingin merubah password</small>
                    @error('passowrd')
                        <div class="invalid-feedback" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                    @enderror
                </div>
            </div>
            <button class="btn btn-primary btn-sm">Simpan</button>
        </form>
    </div>
</div>
@stop