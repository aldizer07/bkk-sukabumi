@extends('layouts.admin.master')
@section('title','Dashboard')
@section('content')
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Perusahaan</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $perusahaan }} perusahaan</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-building fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-success shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Alumni</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $alumni }} orang</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-users fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-6 mb-4">
      <div class="card border-left-info shadow h-100 py-2">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Lowongan</div>
              <div class="row no-gutters align-items-center">
                <div class="col-auto">
                  <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{ $lowongan }} lowongan</div>
                </div>
              </div>
            </div>
            <div class="col-auto">
              <i class="fas fa-newspaper fa-2x text-gray-300"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="row">
  <div class="col-md-12">
    @if(auth()->user()->level == 0)
    <div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">Daftar Admin Sekolah</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th width="180">Nama</th>
                            <th>Email</th>
                            <th>No Telp.</th>
                            <th>Sekolah</th>
                            <th>Bukti</th>
                            <!-- <th>Alamat</th> -->
                            <th width="100">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($listadminsekolah as $item)
                            <tr>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->phonenumber }}</td>
                                <td>{{ $item->sekolah->sekolah }}</td>
                                <td> <a href="{{ route('admin.bukti', $item->id) }}">Lihat Bukti</a> </td>
                                <!-- <td>{{ $item->sekolah->alamat_jalan }}, {{ $item->sekolah->kecamatan }}, {{ $item->sekolah->kabupaten_kota }}, {{ $item->sekolah->propinsi }}</td> -->
                                <td>
                                    <a href="{{ route('admin.acc', $item->id) }}" class="btn btn-success btn-sm btn-circle"><i class="fa fa-check"></i></a>
                                    <a href="{{ route('admin.reject', $item->id) }}" class="btn btn-danger btn-sm hapus btn-circle"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7">Tidak ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            {!! $listadminsekolah->links() !!}
        </div>
    </div>
    @endif
  </div>
</div>

@endsection
@section('js')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.hapus').on('click', function(){
                swal({
                    title: "Apa anda yakin?",
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete) {
                        $.ajax({
                            url: $(this).attr('href'),
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success:function(){
                                swal("Data berhasil dihapus", {
                                    icon: "success",
                                }).then((willDelete) => {
                                    window.location="{{ route('admin.index') }}"
                                });
                            }
                        });
                    } else {
                        swal("Data aman");
                    }
                });

                return false;
            });
        });
    </script>
@endsection
