@extends('layouts.admin.master')
@section('title','Daftar Pelamar')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('wysihtml5/bootstrap-wysihtml5.css') }}">
@endsection
@section('content')
@if (Session::has('message'))
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        {{ Session::get('message') }}
        @php 
            Session::forget('message')
        @endphp
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>                        
@endif
<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#baru" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
      <h6 class="m-0 font-weight-bold text-primary">Pelamar Baru</h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse show" id="baru">
      <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Sekolah</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pelamar_proses as $item)
                        <tr>
                            <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#show{{ $item->id }}">
                                {{ $item->student->nisn }}
                            </button></td>
                            <td>{{ $item->student->name }}</td>
                            <td>{{ (empty($item->student->gender)) ? '-' : $item->student->gender }}</td>
                            <td>{{ $item->student->sekolah->sekolah }}</td>
                            <td>
                                <a href="{{ url('/admin/apply/accepted/'.$item->id) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                                <a href="{{ url('/admin/apply/rejected/'.$item->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
      </div>
    </div>
    @foreach ($pelamar_proses as $item)
<div class="modal fade" id="show{{ $item->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">{{ $item->student->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <img src="{{ asset('storage/photo/'.$item->student->photo) }}" alt="{{ $item->student->name }}" width="100" height="100">
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>NISN</th>
                                <td>:</td>
                                <td>{{ $item->student->nisn }}</td>
                            </tr>
                            <tr>
                                <th>Nama Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->name }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td>{{ $item->student->gender }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal Lahir</th>
                                <td>:</td>
                                <td>{{ $item->student->place_of_birth }}, {{ $item->student->date_of_birth }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->address }}</td>
                            </tr>
                            <tr>
                                <th>Email Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->email }}</td>
                            </tr>
                            <tr>
                                <th>Alumni Sekolah</th>
                                <td>:</td>
                                <td>{{ $item->student->sekolah->sekolah }}</td>
                            </tr>
                            <tr>
                                <th>Curiculum Vitae</th>
                                <td>:</td>
                                <td>
                                    @if (empty($item->student->cv))
                                        -
                                    @else
                                    <a href="{{ asset('storage/cv/'.$item->student->cv) }}" target="blank">Lihat disini</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
        </div>
    </div>
    </div>
</div>
@endforeach
</div>
<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#lolostest" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
      <h6 class="m-0 font-weight-bold text-primary">Lolos Test</h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse" id="lolostest">
      <div class="card-body">
          <div class="mb-3">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#postjadwal">
                Post Jadwal
            </button>
          </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Sekolah</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pelamar_test as $item)
                        <tr>
                            <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#show{{ $item->id }}">
                                {{ $item->student->nisn }}
                            </button></td>
                            <td>{{ $item->student->name }}</td>
                            <td>{{ (empty($item->student->gender)) ? '-' : $item->student->gender }}</td>
                            <td>{{ $item->student->sekolah->sekolah }}</td>
                            <td>
                                <a href="{{ route('test.accepted',$item->id) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                                <a href="{{ route('test.rejected',$item->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
      </div>
    </div>
    @foreach ($pelamar_test as $item)
<div class="modal fade" id="show{{ $item->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">{{ $item->student->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <img src="{{ asset('storage/photo/'.$item->student->photo) }}" alt="{{ $item->student->name }}" width="100" height="100">
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>NISN</th>
                                <td>:</td>
                                <td>{{ $item->student->nisn }}</td>
                            </tr>
                            <tr>
                                <th>Nama Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->name }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td>{{ $item->student->gender }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal Lahir</th>
                                <td>:</td>
                                <td>{{ $item->student->place_of_birth }}, {{ $item->student->date_of_birth }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->address }}</td>
                            </tr>
                            <tr>
                                <th>Email Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->email }}</td>
                            </tr>
                            <tr>
                                <th>Alumni Sekolah</th>
                                <td>:</td>
                                <td>{{ $item->student->sekolah->sekolah }}</td>
                            </tr>
                            <tr>
                                <th>Curiculum Vitae</th>
                                <td>:</td>
                                <td>
                                    @if (empty($item->student->cv))
                                        -
                                    @else
                                    <a href="{{ asset('storage/cv/'.$item->student->cv) }}" target="blank">Lihat disini</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
        </div>
    </div>
    </div>
</div>
@endforeach
</div>
<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#loloswawancara" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
      <h6 class="m-0 font-weight-bold text-primary">Lolos Wawancara</h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse" id="loloswawancara">
      <div class="card-body">
          <div class="mb-3">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#postjadwal">
                Post Jadwal
            </button>
          </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>NISN</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Sekolah</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pelamar_wawancara as $item)
                        <tr>
                            <td><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#show{{ $item->id }}">
                                {{ $item->student->nisn }}
                            </button></td>
                            <td>{{ $item->student->name }}</td>
                            <td>{{ (empty($item->student->gender)) ? '-' : $item->student->gender }}</td>
                            <td>{{ $item->student->sekolah->sekolah }}</td>
                            <td>
                                @if ($item->status != 'selesai')
                                <a href="{{ route('wawancara.accepted',$item->id) }}" class="btn btn-success btn-sm"><i class="fa fa-check"></i></a>
                                <a href="{{ route('wawancara.rejected',$item->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i></a>                                    
                                @endif
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
      </div>
    </div>
    @foreach ($pelamar_wawancara as $item)
<div class="modal fade" id="show{{ $item->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">{{ $item->student->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <img src="{{ asset('storage/photo/'.$item->student->photo) }}" alt="{{ $item->student->name }}" width="100" height="100">
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>NISN</th>
                                <td>:</td>
                                <td>{{ $item->student->nisn }}</td>
                            </tr>
                            <tr>
                                <th>Nama Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->name }}</td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td>:</td>
                                <td>{{ $item->student->gender }}</td>
                            </tr>
                            <tr>
                                <th>Tempat, Tanggal Lahir</th>
                                <td>:</td>
                                <td>{{ $item->student->place_of_birth }}, {{ $item->student->date_of_birth }}</td>
                            </tr>
                            <tr>
                                <th>Alamat Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->address }}</td>
                            </tr>
                            <tr>
                                <th>Email Alumni</th>
                                <td>:</td>
                                <td>{{ $item->student->email }}</td>
                            </tr>
                            <tr>
                                <th>Alumni Sekolah</th>
                                <td>:</td>
                                <td>{{ $item->student->sekolah->sekolah }}</td>
                            </tr>
                            <tr>
                                <th>Curiculum Vitae</th>
                                <td>:</td>
                                <td>
                                    @if (empty($item->student->cv))
                                        -
                                    @else
                                    <a href="{{ asset('storage/cv/'.$item->student->cv) }}" target="blank">Lihat disini</a>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        {{-- <button type="button" class="btn btn-primary">Understood</button> --}}
        </div>
    </div>
    </div>
</div>
@endforeach
</div>

<!-- Modal -->
<div class="modal fade" id="postjadwal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="postjadwal" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Post Jadwal</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('post.store') }}" method="post">
                @csrf
                <input type="hidden" value="{{ $perusahaan->company->id }}" name="perusahaan_id">
                <input type="hidden" name="user_id" value="{{ auth()->user()->id}}">
                <div class="form-group">
                    <label for="title">Judul</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group">
                    <label for="isi">Isi</label>
                    <textarea id="isi" placeholder="Enter text ..." class="form-control" name="description"></textarea>
                </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button class="btn btn-primary">Simpan</button>
        </form>
        </div>
      </div>
    </div>
</div>
@endsection