@extends('layouts.admin.master')
@section('title', $company->name)
@section('content')
    <div class="row">
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('company.index') }}" class="btn btn-circle"><i class="fa fa-arrow-left"></i></a>  {{ $company->name }}</h6>
                </div>
                <div class="card-body">
                    @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
                    <form action="{{ route('company.update', $company->id) }}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="name">Nama Perusahaan</label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ $company->name }}">
                            @error('name')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="address">Alamat Perusahaan</label>
                            <input type="text" name="address" id="address" class="form-control @error('address') is-invalid @enderror" value="{{ $company->address }}">
                            @error('address')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi Perusahaan</label>
                            <textarea name="description" id="description" cols="5" rows="5" class="form-control @error('description') is-invalid @enderror ">{{ $company->description }}</textarea>
                            @error('description')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        @if (auth()->user()->level == 0)
                        <div class="form-group">
                            <label for="">Lowongan Untuk</label>
                            <select name="user_id" id="" class="form-control" required>
                                <option value="">Pilih Sekolah</option>
                                @foreach ($sekolah as $item)
                                    <option value="{{ $item->id }}" {{ $company->user_id == $item->id ? 'selected' : '' }}>{{ $item->sekolah->sekolah }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <button class="btn btn-primary">Simpan</button>
                    </form>
                </div>
              </div>
        </div>
    </div>
@endsection