@extends('layouts.admin.master')
@section('title','Daftar Perusahaan')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Perusahaan</h6>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <a href="{{ route('company.create') }}" class="btn btn-sm btn-success">Tambah Data</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Perusahaan</th>
                                    <th>Alamat Perusahaan</th>
                                    <th>Deskripsi Perusahaan</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($companies as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->address }}</td>
                                        <td>{{ $item->description }}</td>
                                        <td>
                                            <a href="{{ route('company.edit',$item->id) }}" class="btn btn-warning btn-sm btn-circle"><i class="fa fa-edit"></i></a>
                                            <a href="{{ route('company.destroy',$item->id) }}" class="btn btn-danger btn-sm btn-circle hapus"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Tidak ada data</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                    {!! $companies->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.hapus').on('click', function(){
                swal({
                    title: "Apa anda yakin?",
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete) {
                        $.ajax({
                            url: $(this).attr('href'),
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success:function(){
                                swal("Data berhasil dihapus", {
                                    icon: "success",
                                }).then((willDelete) => {
                                    window.location="{{ route('company.index') }}"
                                });
                            }
                        });
                    } else {
                        swal("Data aman");
                    }
                });

                return false;
            });
        }); 
    </script>
@endsection