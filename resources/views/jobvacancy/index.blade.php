@extends('layouts.admin.master')
@section('title','Bursa Kerja')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Bursa Kerja</h6>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <a href="{{ route('bursakerja.create') }}" class="btn btn-sm btn-success">Tambah Data</a>
        </div>
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Perusahaan</th>
                        <th>Judul</th>
                        <th>Tanggal Berakhir</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($job_vacancies as $item)
                        <tr>
                            <td>{{ $item->company->name }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ date('d F Y',strtotime($item->end_date)) }}</td>
                            <td>
                                <a href="{{ route('pelamar',$item->id) }}" class="btn btn-success btn-sm btn-circle"><i class="fa fa-users"></i></a>
                                <button type="button" class="btn btn-primary btn-sm btn-circle" data-toggle="modal" data-target="#show{{ $item->id }}">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <a href="{{ route('bursakerja.edit', $item->id) }}" class="btn btn-warning btn-sm btn-circle"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('bursakerja.destroy', $item->id) }}" class="btn btn-danger btn-sm hapus btn-circle"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @empty
                        
                    @endforelse
                </tbody>
            </table>
        </div>
        {!! $job_vacancies->links() !!}
    </div>
</div>
<!-- Modal -->
@foreach ($job_vacancies as $job)
    

<div class="modal fade" id="show{{ $job->id }}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">{{ $job->company->name }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <h5><b>{{ $job->title }}</b></h5>
            <small>Dibuat pada {{ date('d F Y', strtotime($job->created_at)) }}</small>
            <p>Range Gaji Rp.{{ number_format($job->start_salary) }} - Rp.{{ number_format($job->end_salary) }} <br> {{ $job->position }} <br> {{ $job->location }}</p>
            <hr>
            <p>{{ $job->description }}</p>
            <hr>
            <p>Closed <strong>{{ date('d F Y', strtotime($job->end_date)) }}</strong></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          
        </div>
      </div>
    </div>
  </div>
@endforeach
@endsection
@section('js')
    <script src="{{ asset('vendor/sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.hapus').on('click', function(){
                swal({
                    title: "Apa anda yakin?",
                    text: "Data yang dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(willDelete) {
                        $.ajax({
                            url: $(this).attr('href'),
                            type: "DELETE",
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success:function(){
                                swal("Data berhasil dihapus", {
                                    icon: "success",
                                }).then((willDelete) => {
                                    window.location="{{ route('bursakerja.index') }}"
                                });
                            }
                        });
                    } else {
                        swal("Data aman");
                    }
                });

                return false;
            });
        }); 
    </script>
@endsection