@extends('layouts.admin.master')
@section('title',$job_vacancy->title)
@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Ubah Bursa Kerja</h6>
            </div>
            <div class="card-body">
                @if (Session::has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        @php 
                            Session::forget('message')
                        @endphp
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>                        
                    @endif
                <form action="{{ route('bursakerja.update', $job_vacancy->id) }}" method="post">
                    @csrf
                    @method('put')
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="company_id">Perusahaan</label>
                            <select name="company_id" id="company_id" class="form-control form-control-sm @error('company_id') is-invalid @enderror">
                                @foreach ($company as $item)
                                    <option value="{{ $item->id }}" {{ $job_vacancy->company_id == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                            @error('company_id')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="title">Judul</label>
                            <input type="text" name="title" class="form-control form-control-sm @error('title') is-invalid @enderror" value="{{ $job_vacancy->title }}">
                            @error('title')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12 mb-3">
                            <label for="description">Deskripsi</label>
                            <textarea name="description" id="description" cols="5" rows="5" class="form-control form-control-sm">{{ $job_vacancy->description }}</textarea>
                            @error('description')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="start_salary">Gaji Mulai</label>
                            <input type="text" name="start_salary" class="form-control form-control-sm @error('start_salary') is-invalid @enderror" value="{{ $job_vacancy->start_salary }}">
                            @error('start_salary')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="end_salary">Gaji Sampai</label>
                            <input type="text" name="end_salary" class="form-control form-control-sm @error('end_salary') is-invalid @enderror" value="{{$job_vacancy->end_salary }}">
                            @error('end_salary')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4 mb-3">
                            <label for="position">Posisi</label>
                            <input type="text" name="position" class="form-control form-control-sm @error('position') is-invalid @enderror" value="{{ $job_vacancy->position }}">
                            @error('position')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="location">Lokasi</label>
                            <input type="text" name="location" class="form-control form-control-sm @error('location') is-invalid @enderror" value="{{ $job_vacancy->location }}">
                            @error('location')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="end_date">Batas Waktu</label>
                            <input type="date" name="end_date" class="form-control form-control-sm @error('end_date') is-invalid @enderror" value="{{ $job_vacancy->end_date }}">
                            @error('location')
                                <div class="invalid-feedback" role="alert">
                                    <span>{{ $message }}</span>
                                </div>
                            @enderror
                        </div>
                    </div>
                    <button class="btn btn-sm btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection