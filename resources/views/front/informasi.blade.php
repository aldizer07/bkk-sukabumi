@extends('layouts.front.app')
@section('title','Informasi Lamaran')
@section('content')
    <div class="row" style="font-size: 14px;">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4>Informasi Lamaran</h4>
                    <hr>
                    <div class="row">
                        @forelse ($apply as $item)
                        <div class="col-md-3 mb-3">
                            <h6><b><a href="#" id="title{{ $item->id }}" style="text-decoration: none;">{{ $item->jobvacancy->title }} </a></b> <span class="{{ $item->color_status }}">{{ $item->status }}</span></h6>
                            <table>
                                <tr>
                                    <td>
                                        <i class="fa fa-map-marker"></i> {{ $item->jobvacancy->location }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-dollar-sign"></i> Rp.{{ number_format($item->jobvacancy->start_salary) }} - Rp.{{ number_format($item->jobvacancy->end_salary) }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-building"></i> {{ $item->jobvacancy->company->name }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        @empty
                            <p>Tidak memiliki lowongan</p>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop