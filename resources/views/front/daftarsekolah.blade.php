@extends('layouts.front.app')
@section('title','Halaman Login - Register')
@section('css')
<link rel="stylesheet" href="{{ asset('select2/css/select2.min.css') }}">
<style>
    .box {
        margin: 0 20px 0 20px;
        background-color: #ffffff;
        border-radius: 10px;
        padding: 30px;
    }
    .border {
        border: 1px solid #ededed; padding:30px;
    }
    .form-control {
        border-radius: 0;
    }
    label {
        color: #8e8e8e;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="box">
        @if (Session::has('message'))
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                {{ Session::get('message') }}
                @php
                    Session::forget('message')
                @endphp
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
        @endif
        @if (Session::has('error'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ Session::get('error') }}
                @php
                    Session::forget('error')
                @endphp
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
        @endif
        <div class="row" style="margin-bottom: 80px;">
            <div class="col-md-12 col-lg-4 border">
                <h3>Login</h3>
                <hr>
                <form action="{{ route('loginprocess.admin') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="nisn">Email</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nisn">Password</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button class="btn btn-primary">Login</button>
                </form>
                <br>
            </div>
            <div class="col-md-12 col-lg-8 border">
                <h3>Daftar</h3>
                <hr>
                <form action="{{ route('daftarsekolah.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4 mb-3">
                            <label for="nisn">Nama Lengkap</label>
                            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="phonenumber">No. Tlp</label>
                            <input type="text" name="phonenumber" id="phonenumber" class="form-control @error('phonenumber') is-invalid @enderror" value="{{ old('phonenumber') }}">
                            @error('phonenumber')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="email_register">Email</label>
                            <input type="email" name="email_register" id="email_register" class="form-control @error('email_register') is-invalid @enderror" value="{{ old('email_register') }}">
                            @error('email_register')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 mb-3">
                            <label for="password_register">Password</label>
                            <input type="password" name="password_register" id="password_register" class="form-control @error('password_register') is-invalid @enderror" >
                            @error('password_register')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="sekolah">Sekolah</label>
                            <select name="school_id" id="school_id" class="form-control @error('school_id') is-invalid @enderror">
                                @foreach ($sekolah as $item)
                                    <option value="{{ $item->id }}">{{ $item->sekolah }}</option>
                                @endforeach
                            </select>
                            @error('school_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                      <label for="cv">Upload Surat Izin BKK dari Dinas</label>
                      <input type="file" name="file" class="form-control @error('file') is-invalid @enderror">
                      @error('file')
                        <div class="invalid-feedback" role="alert">
                            <span>{{ $message }}</span>
                        </div>
                        @enderror
                      <small class="text-danger">* File berupa PDF</small>
                    </div>
                    <button class="btn btn-primary">Daftar</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{ asset('select2/js/select2.min.js') }}"></script>

<script>
    $(document).ready(function(){

        $('#school_id').select2();
    });
</script>
@endsection
