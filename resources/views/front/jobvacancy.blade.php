@extends('layouts.front.app')
@section('title','Cari Lowongan')
@section('content')
    <div class="row" style="font-size: 14px;">
        <div class="col-lg-5 col-md-12 col-xs-12 mb-3">
            <div class="card">
                <div class="card-body p-4">
                    <h3>Daftar Lowongan</h3>
                    <hr>
                    @if (Session::has('message'))
                        <div class="alert alert-info alert-dismissible fade show" role="alert">
                            {{ Session::get('message') }}
                            @php 
                                Session::forget('message')
                            @endphp
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>                        
                    @endif
                    @forelse ($job as $item)
                        <div class="row">
                            <div class="col-md-12">
                                <h6><b><a href="#" id="title{{ $item->id }}" style="text-decoration: none;">{{ $item->title }}</a></b></h6>
                                <input type="hidden" value="{{ url('/api/jobvacancy/'.$item->id.'/'.auth()->guard('students')->user()->id) }}" id="link{{ $item->id }}">
                                <table>
                                    <tr>
                                        <td>
                                            <i class="fa fa-map-marker"></i> {{ $item->location }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-dollar-sign"></i> Rp.{{ number_format($item->start_salary) }} - Rp.{{ number_format($item->end_salary) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <i class="fa fa-building"></i> {{ $item->company->name }}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <hr>
                    @empty
                        <p>Tidak ada lowongan saat ini</p>
                    @endforelse

                    {!! $job->links() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-12 col-xs-12">
            <div class="card">
                <div class="card-body p-4" id="detail" style="height: 570px;">
                    <h5 class="text-center" style="margin-top: 200px;">Detail Informasi Lowongan akan tampil disini</h5>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
   <script>
       $(document).ready(function(){
           @foreach($job as $j)
            $('#title{{ $j->id }}').on('click', function(){
                $.ajax({
                    url: $('#link{{ $j->id }}').val(),
                    type: 'GET',
                    success:function(res){
                        console.log(res.check);
                        var btnApply = '';
                        let tanggal = new Date(res.data.created_at);
                        let formatDate = tanggal.getDate() + "-" + (tanggal.getMonth() + 1) + "-" + tanggal.getFullYear()
                        if (res.check == null) {
                            btnApply = '<a href="{{ route('apply',$j->id) }}" class="btn btn-success">Lamar</a>'
                        }else {
                            btnApply = '<p>Anda sudah melamar</p>'
                        }
                        $('#detail').html(`
                            <h5><b>`+res.data.title+`</b></h5>
                            <p>`+res.data.company.name.toUpperCase()+` <br>
                            `+res.data.location+` <br>
                            Rp `+formatNumber(res.data.start_salary)+` - Rp `+formatNumber(res.data.end_salary)+` <br>
                            <span class="text-danger">Ditutup pada `+res.data.end_date+`</span> <br>
                            <span class="text-muted">Posted on `+formatDate+`</span>
                            </p>
                            
                            `+btnApply+`

                            <hr>

                            <p><b>Deskripsi Pekerjaan</b></p>

                            <p>`+res.data.description+`</p>
                        `)
                    }
                });
            })
            @endforeach
       });
       function formatNumber(num) {
		  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
		}
   </script>
@endsection