@extends('layouts.front.app')
@section('title','Post')
@section('content')
    <div class="row">
        <div class="col-md-8">
            @forelse ($post as $item)
            <div class="card">
                <div class="card-body">
                    <h4>{{ $item->title }}</h4>
                    <i class="fa fa-building"></i> {{ $item->company->name }}
                    <p>{{ $item->description }}</p>
                </div>
            </div>
            @empty
            <div class="card">
                <div class="card-body">
                    <p>Tidak ada informasi</p>
                </div>
            </div>
            @endforelse
        </div>
    </div>
@endsection