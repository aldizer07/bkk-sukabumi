@extends('layouts.front.app')
@section('title','Bursa Kerja Khusus SMK')
@section('content')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
      <h1 class="display-4">Bursa Kerja Khusus</h1>
      <p class="lead">Daftarkan Sekolah Anda dan Bergabung Bersama Kami</p>
      <a href="{{ route('daftarsekolah') }}" class="btn btn-success">Daftar Sekolah</a>
      <a href="{{ route('login') }}" class="btn btn-warning">Daftar Alumni</a>
  </div>
</div>
<div class="container text-center" style="margin-bottom: 100px;">
  <h2>Tentang Kami</h2>
  <hr>
  <p style="font-size: 30px">Bursa Kerja Khusus SMK adalah tempat BKK SMK Se-Kota Sukabumi yang diperuntukan untuk para alumni masing-masing SMK</p>
</div>
<div class="container mb-5">
  <h2 style="font-weight: 2" class="text-center">{{ strtoupper('Daftar Sekolah') }}</h2><hr>
  <div class="row">
    @forelse ($sekolah as $item)
    <div class="col-md-4 mb-3">
        <h5>{{ $item->sekolah->sekolah }}</h5>
        <table>
            <tr>
              <td><i class="fa fa-envelope"></i> &nbsp; {{ $item->email }}</td>
            </tr>
            <tr>
              <td><i class=" fa fa-building"></i> &nbsp; {{ $item->sekolah->alamat_jalan }}, {{ $item->sekolah->kecamatan }}, {{ $item->sekolah->kabupaten_kota }}, {{ $item->sekolah->propinsi }}</td>
            </tr>
            <tr>
              <td><i class="fa fa-phone"> &nbsp; {{ (empty($item->phonenumber)) ? '-' : $item->phonenumber }}</i></td>
            </tr>
        </table>
    </div>
    
    @empty
        <p>Tidak ada sekolah yang terdaftar</p>
    @endforelse
</div>
</div>
@endsection