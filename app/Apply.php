<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apply extends Model
{
    protected $guarded = [];

    public function jobvacancy()
    {
        return $this->belongsTo(JobVacancy::class,'job_vacancy_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }
}
