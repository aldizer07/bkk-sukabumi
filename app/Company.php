<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    public function jobVacancy()
    {
        return $this->hasMany(JobVacancy::class,'company_id');
    }

    public function post()
    {
        return $this->hasMany(Post::class,'company_id');
    }
}
