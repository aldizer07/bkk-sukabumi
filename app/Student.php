<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\School;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Student extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    // protected $primaryKey = 'sekolah_id';
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    public function sekolah()
    {
        return $this->belongsTo(School::class, 'school_id');
    }
    public function apply()
    {
        return $this->hasMany(Apply::class);
    }
}
