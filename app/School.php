<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    // protected $table = 'sekolah';
    // protected $primaryKey = 'id';

    public function student()
    {
        return $this->hasMany(Student::class);
    }

    public function admin()
    {
        return $this->hasOne(User::class);
    }
}
