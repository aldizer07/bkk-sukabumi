<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && auth()->guard($guard)->user()->status == 1) {
            switch ($guard) {
                case 'users':
                    return redirect()->route('home.admin');
                    break;

                case 'students':
                    return redirect()->route('jobvacancy');
                    break;
            }
        }

        return $next($request);
    }
}
