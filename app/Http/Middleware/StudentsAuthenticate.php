<?php

namespace App\Http\Middleware;

use Closure;

class StudentsAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->guard('students')->check() && !auth()->guard('students')->user()->status == 1) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
