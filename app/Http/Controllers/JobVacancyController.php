<?php

namespace App\Http\Controllers;

use App\Apply;
use Illuminate\Http\Request;
use App\JobVacancy;
use App\Company;
use App\Post;
use App\User;

class JobVacancyController extends Controller
{
    public function index()
    {
        $job_vacancies = JobVacancy::with('company');

        // if (auth()->user()->level == 0) {
            $job_vacancies = $job_vacancies->where('user_id', auth()->user()->id );
        // }

        $job_vacancies = $job_vacancies->orderBy('created_at','DESC')->paginate(10);

        // return response()->json($job_vacancies);
        return view('jobvacancy.index',compact('job_vacancies'));
    }

    public function create()
    {
        $company = Company::get();
        $user = User::with('sekolah')->where('level',1)->get();
        return view('jobvacancy.create',compact('company','user'));
    }

    public function store(Request $request)
    {
        $arr = $this->validate($request, [
            'company_id'        => 'required|exists:companies,id',
            'user_id'           => 'required|exists:users,id',
            'title'             => 'required|string',
            'end_date'          => 'required|date',
            'start_salary'      => 'required|numeric',
            'end_salary'        => 'required|numeric',
            'description'       => 'required|string',
            'position'          => 'required|string',
            'location'          => 'required|string'
        ]);

        // dd($arr);

        JobVacancy::create([
            'company_id'        => $request->company_id,
            'user_id'           => $request->user_id,
            'title'             => $request->title,
            'end_date'          => $request->end_date,
            'start_salary'      => $request->start_salary,
            'end_salary'        => $request->end_salary,
            'description'       => $request->description,
            'position'          => $request->position,
            'location'          => $request->location,
            'school_id'         => auth()->user()->school_id
        ]);

        return back()->with('message','Berhasil menambahkan data');
    }

    public function edit($id)
    {
        $company = Company::get();
        $job_vacancy = JobVacancy::findOrFail($id);
        return view('jobvacancy.edit',compact('company','job_vacancy'));
    }

    public function update(Request $request, $id)
    {
        $job_vacancy = JobVacancy::findOrFail($id);

        $arr = $this->validate($request, [
            'company_id'        => 'required|exists:companies,id',
            'title'             => 'required|string',
            'end_date'          => 'required|date',
            'start_salary'      => 'required|numeric',
            'end_salary'        => 'required|numeric',
            'description'       => 'required|string',
            'position'          => 'required|string',
            'location'          => 'required|string'
        ]);

        $job_vacancy->update([
            'company_id'        => $request->company_id,
            'title'             => $request->title,
            'end_date'          => $request->end_date,
            'start_salary'      => $request->start_salary,
            'end_salary'        => $request->end_salary,
            'description'       => $request->description,
            'position'          => $request->position,
            'location'          => $request->location
        ]);

        return back()->with('message','Berhasil merubah data');

    }

    public function destroy($id)
    {
        $job_vacancy = JobVacancy::findOrFail($id);
        $job_vacancy->delete();
    }

    public function pelamar($id)
    {
        $pelamar_proses = Apply::with(['student.sekolah','jobvacancy'])->where('job_vacancy_id',$id)->where('status','Diproses')->orWhere('status','Tidak Lolos')->paginate(10);

        $pelamar_test = Apply::with(['student.sekolah','jobvacancy'])->where('job_vacancy_id',$id)->where('status','Test')->orWhere('status','Tidak Lolos')->paginate(10);

        $pelamar_wawancara = Apply::with(['student.sekolah','jobvacancy'])->where('job_vacancy_id',$id)->where('status','Wawancara')->orWhere('status','Tidak Lolos')->orWhere('status','selesai')->paginate(10);
        $perusahaan = JobVacancy::with('company')->findOrFail($id);

        // dd($perusahaan);

        return view('pelamar',compact('pelamar_proses','pelamar_test','pelamar_wawancara','perusahaan'));
    }

    public function accepted($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'Test',
            'color_status' => 'badge badge-info'
        ]);

        return back()->with('message','Berhasil');
    }
    public function rejected($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'Tidak Lolos',
            'color_status' => 'badge badge-danger'
        ]);

        return back()->with('message','Berhasil');
    }
    
    public function postStore(Request $request)
    {
        Post::create([
            'title'   => $request->title,
            'description'   => $request->description,
            'company_id'    => $request->perusahaan_id,
            'user_id'       => $request->user_id
        ]);

        return back()->with('message','Post Berhasil');
    }

    public function testAccepted($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'Wawancara',
            'color_status' => 'badge badge-success'
        ]);

        return back()->with('message','Berhasil');
    }
    public function testRejected($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'Tidak Lolos',
            'color_status' => 'badge badge-danger'
        ]);

        return back()->with('message','Berhasil');
    }
    public function wawancaraAccepted($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'selesai',
            'color_status' => 'badge badge-success'
        ]);

        return back()->with('message','Berhasil');
    }
    public function wawancaraRejected($id)
    {
        $apply = Apply::findOrFail($id);
        $apply->update([
            'status'    => 'Tidak Lolos',
            'color_status' => 'badge badge-danger'
        ]);

        return back()->with('message','Berhasil');
    }
}
