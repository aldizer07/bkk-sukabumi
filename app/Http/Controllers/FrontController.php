<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JobVacancy;
use App\Apply;
use App\Post;
use App\School;
use App\Student;
use App\User;
use Illuminate\Support\Facades\App;

class FrontController extends Controller
{
    public function index()
    {
        $sekolah = User::with('sekolah')->where('level',1)->orderBy('name')->get();
        // dd($sekolah);
        return view('welcome',compact('sekolah'));
    }

    public function daftarSekolah()
    {
        $sekolah = School::select('id','sekolah')->orderBy('id','ASC')->get();
        return view('front.daftarsekolah',compact('sekolah'));

    }

    public function storeDaftarSekolah(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|string',
            'phonenumber'   => 'required|numeric|min:6',
            'email_register'         => 'required|unique:users,email',
            'password_register'      => 'required|min:8',
            'school_id'     => 'required|unique:users,school_id',
            'file'          => 'required|mimes:pdf'
        ]);

        $filename = '';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = rand().'.'.$file->getClientOriginalExtension();
            $file->storeAs('/public/bukti',$filename);
        }

        User::create([
            'name'      => $request->name,
            'phonenumber'   => $request->phonenumber,
            'email'     => $request->email_register,
            'password'  => bcrypt($request->password_register),
            'level'     => 1,
            'school_id' => $request->school_id,
            'bukti' => $filename
        ]);
         return back()->with('message','Berhasil daftar, silahkan tunggu konfirmasi dari admin');
    }

    public function jobVacancy()
    {
        $job = JobVacancy::with('company')->where('school_id',auth()->guard('students')->user()->school_id)->where('end_date','>=',date('d-m-Y'))->orderBy('created_at')->paginate(3);

        // dd($job);
        return view('front.jobvacancy',compact('job'));
    }

    public function apply($id)
    {
        Apply::create([
            'job_vacancy_id'        => $id,
            'student_id'            => auth()->guard('students')->user()->id,
            'status'                => 'Diproses',
            'color_status'          => 'badge badge-primary'
        ]);

        return back()->with('message','Berhasil melamar');
    }

    public function informationApply()
    {
        $apply = Apply::with('jobvacancy','student')->where('student_id',auth()->guard('students')->user()->id)->orderBy('created_at','DESC')->paginate(10);


        // return response()->json($apply);

        return view('front.informasi',compact('apply'));
    }

    public function profile()
    {
        $id = auth()->guard('students')->user()->id;
        $profile = Student::findOrFail($id);

        return view('profile',compact('profile'));
    }

    public function profileUpdate(Request $request,$id)
    {

        $student = Student::findOrFail($id);
        // dd($student);
        // $this->validate($request, [
        //     'nisn'              => 'required|numeric|min:10|unique:students,nisn,'. $id,
        //     'name'              => 'required|string',
        //     'gender'            => 'required|string',
        //     'place_of_birth'    => 'required|string',
        //     'date_of_birth'     => 'required|date',
        //     'address'           => 'required|string',
        //     'email'             => 'required|email',
        //     'cv'                => 'nullable|mimes:doc,docx,pdf|max:1024',
        //     'photo'             => 'nullable|mimes:png,jpg,jpeg|max:1024',
        // ]);
        // dd($request->place_of_birth);
        $password = ($request->password != 'null') ? bcrypt($request->password) : $student->password ;
        $cv = $student->cv;
        $photo = $student->photo;
        if ($request->hasFile('cv')) {
            $fileCv = $request->file('cv');
            if ($cv != null) {
                unlink(storage_path('app/public/cv/').$cv);
            }
            $cv = rand().'.'.$fileCv->getClientOriginalExtension();
            $fileCv->storeAs('/public/cv',$cv);

        }
        if ($request->hasFile('foto')) {
            $filePhoto = $request->file('foto');
            if ($photo != null) {
                unlink(storage_path('app/public/photo/').$photo);
            }
            $photo = rand().'.'.$filePhoto->getClientOriginalExtension();
            $filePhoto->storeAs('/public/photo',$photo);
        }

        $student->update([
            'nisn'              => $request->nisn,
            'name'              => $request->name,
            'gender'            => $request->gender,
            'place_of_birth'    => $request->place_of_birth,
            'date_of_birth'     => $request->date_of_birth,
            'address'           => $request->address,
            'email'             => $request->email,
            'password'          => $password,
            'cv'                => $cv,
            'photo'             => $photo
        ]);

        return back()->with('message','Berhasil merubah data');
    }

    public function postingan()
    {
        $school_id = auth()->guard('students')->user()->school_id;
        $post = Post::with('company','user.sekolah')->whereHas('user', function($query) use($school_id){
            $query->where('school_id',$school_id);
        })->paginate(10);

        return view('front.post',compact('post'));
    }
}
