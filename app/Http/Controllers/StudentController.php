<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\School;
use App\User;

class StudentController extends Controller
{
    public function index()
    {
        
        $students = Student::with('sekolah');
        $user = User::with('sekolah')->findOrFail(auth()->user()->id);
        if (auth()->user()->level == 1) {
            $students =$students->where('school_id',$user->sekolah->id);
        }
        // return response()->json($students);
        $students = $students->orderBy('nisn','ASC')->paginate(10);

        return view('student.index',compact('students'));
    }

    public function create()
    {
        $sekolah = School::select('id','sekolah')->get();
        $getSekolah = User::with('sekolah')->findOrFail(auth()->user()->id);
        // dd($getSekolah);
        return view('student.create',compact('sekolah','getSekolah'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nisn'              => 'required|numeric|unique:students|min:10',
            'name'              => 'required|string',
            'gender'            => 'required|string',
            'place_of_birth'    => 'required|string',
            'date_of_birth'     => 'required|date',
            'address'           => 'required|string',
            'email'             => 'required|email',
            'npsn'              => 'required|string',
            'cv'                => 'nullable|mimes:doc,docx,pdf|max:1024',
            'photo'                => 'nullable|mimes:png,jpg,jpeg|max:1024',
        ]);

        // $password = bcrypt($request->nisn);

        $cv = '';
        $photo = '';

        if ($request->hasFile('cv')) {
            $fileCv = $request->file('cv');
            $cv = rand().'.'.$fileCv->getClientOriginalExtension();
            $fileCv->storeAs('/public/cv',$cv);
        }

        if ($request->hasFile('photo')) {
            $filePhoto = $request->file('photo');
            $photo = rand().'.'.$filePhoto->getClientOriginalExtension();
            $filePhoto->storeAs('/public/photo',$photo);
        }

        Student::create([
            'nisn'              => $request->nisn,
            'name'              => $request->name,
            'gender'            => $request->gender,
            'place_of_birth'    => $request->place_of_birth,
            'date_of_birth'     => $request->date_of_birth,
            'address'           => $request->address,
            'email'             => $request->email,
            'password'          => $request->nisn,
            'school_id'              => $request->npsn,
            'cv'                => $cv,
            'photo'             => $photo
        ]);

        return back()->with('message','Berhasil menambahkan data');
    }

    public function edit($id)
    {
        $sekolah = School::select('id','sekolah')->get();
        $student = Student::findOrFail($id);

        // dd($student);
        return view('student.edit',compact('student','sekolah'));
    }

    public function update(Request $request, $id)
    {
        $student = Student::findOrFail($id);
        $this->validate($request, [
            'nisn'              => 'required|numeric|min:10|unique:students,nisn,'. $id,
            'name'              => 'required|string',
            'gender'            => 'required|string',
            'place_of_birth'    => 'required|string',
            'date_of_birth'     => 'required|date',
            'address'           => 'required|string',
            'email'             => 'required|email',
            'npsn'              => 'required|string',
            'cv'                => 'nullable|mimes:doc,docx,pdf|max:1024',
            'photo'             => 'nullable|mimes:png,jpg,jpeg|max:1024',
        ]);

        $password = ($request->password != 'null') ? bcrypt($request->password) : $student->password ;
        $cv = $student->cv;
        $photo = $student->photo;
        if ($request->hasFile('cv')) {
            $fileCv = $request->file('cv');
            if ($cv != null) {
                unlink(storage_path('app/public/cv/').$cv);
            }
            $cv = rand().'.'.$fileCv->getClientOriginalExtension();
            $fileCv->storeAs('/public/cv',$cv);
            
        }
        if ($request->hasFile('photo')) {
            $filePhoto = $request->file('photo');
            if ($photo != null) {
                unlink(storage_path('app/public/photo/').$photo);
            }
            $photo = rand().'.'.$filePhoto->getClientOriginalExtension();
            $filePhoto->storeAs('/public/photo',$photo);
        }

        $student->update([
            'nisn'              => $request->nisn,
            'name'              => $request->name,
            'gender'            => $request->gender,
            'place_of_birth'    => $request->place_of_birth,
            'date_of_birth'     => $request->date_of_birth,
            'address'           => $request->address,
            'email'             => $request->email,
            'password'          => $password,
            'school_id'        => $request->npsn,
            'cv'                => $cv,
            'photo'             => $photo
        ]);

        return back()->with('message','Berhasil merubah data');

    }

    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        unlink(storage_path('app/public/cv/').$student->cv);
        unlink(storage_path('app/public/photo/').$student->photo);
        $student->delete();
    }

}
