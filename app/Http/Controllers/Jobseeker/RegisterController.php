<?php

namespace App\Http\Controllers\Jobseeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
            'nisn_register'          => 'required|numeric|min:10',
            'name'          => 'required|string',
            'date_of_birth' => 'required|date',
            'email'         => 'required|email',
            'npsn'          => 'required',
            'password_register'      => 'required|min:8|string'
        ]);

        Student::create([
            'nisn'          => $request->nisn_register,
            'name'          => $request->name,
            'date_of_birth' => $request->date_of_birth,
            'email'         => $request->email,
            'school_id'     => $request->npsn,
            'password'      => $request->password_register
        ]);

        return back()->with('message','Berhasil registrasi silahkan login untuk menggunakan akun');
    }
}
