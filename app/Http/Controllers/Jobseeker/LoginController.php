<?php

namespace App\Http\Controllers\Jobseeker;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\School;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    
    public function index()
    {
        if (auth()->guard('students')->check()) return redirect(route('jobvacancy'));
        $sekolah = School::select('id','sekolah')->get();

        // dd($getProvince);
        return view('login',compact('sekolah'));
    }

    public function process(Request $request)
    {
        $this->validate($request, [
            'nisn'          => 'required|numeric|min:10|exists:students,nisn',
            'password'    => 'required|string'
        ]);

        $login = $request->only('nisn','password');

        if (Auth::guard('students')->attempt($login)) {
            return redirect()->route('informasi');
        }

        return back()->with('error','NISN atau password salah');

    }

    public function logout()
    {
        auth()->guard('students')->logout();
        return redirect()->route('login');
    }
}
