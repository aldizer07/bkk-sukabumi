<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\School;
use App\Student;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }

    public function loginProcess(Request $request)
    {
        $this->validate($request, [
            'nisn'      => 'required|numeric|min:4|exists:students,nisn',
            'password'  => 'required|string'
        ]);

        $credentials = $request->except(['_token']);

        if (auth()->attempt($credentials)) {
            return redirect()->route('home.admin');
        } else {
            return back()->with('message','Invalid token');
        }
    }

    public function loginAdmin()
    {
        return view('login_admin');
    }

    public function loginProcessAdmin(Request $request)
    {
        // dd($request);
        $request->validate([
            'email'     => 'required|email',
            'password'  => 'required|string'
        ]);


        $credentials = $request->except(['_token']);

        if (auth()->attempt($credentials)) {
            if (auth()->user()->status == 1) {
              return redirect()->route('home.admin');
            }else{
              return back()->with('error','Akun anda belum aktif, anda dapat menghubungi administrator');
            }
        } else {
            return back()->with('message','Invalid token');
        }

    }

    public function logout()
    {
        if (auth()->user()->level == 0) {
            Auth::logout();
            return redirect()->route('login.admin');
        }else{
            Auth::logout();
            return redirect()->route('daftarsekolah');
        }
    }
}
