<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::paginate(10);
        return view('setting.index',compact('settings'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|string',
            'value'     => 'required|string',
        ]);

        Setting::create([
            'name'          => $request->name,
            'value'         => $request->value
        ]);

        return back()->with('message','Berhasil menambah data');
    }

    public function edit($id)
    {
        $setting = Setting::findOrFail($id);

        return view('setting.edit', compact('setting'));
    }

    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail();

        $this->validate($request, [
            'name'      => 'required|string',
            'value'     => 'required|string',
        ]);

        $setting->update([
            'name'          => $request->name,
            'value'         => $request->value
        ]);

        return back()->with('message','Berhasil merubah data');
    }

    public function destroy($id)
    {
        $setting = Setting::findOrFail();

        $setting->delete();
    }

    public function pengaturan()
    {
        $nama = Setting::where('name','Nama Sekolah')->get();

        return $nama;
    }
}
