<?php

namespace App\Http\Controllers;

use App\Apply;
use Illuminate\Http\Request;
use App\School;
use App\JobVacancy;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    public function getCity()
    {
        $getCity = School::select('kode_kab_kota','kabupaten_kota')->where('kode_prop',request()->get('province_code'))->groupBy('kode_kab_kota','kabupaten_kota')->orderBy('kode_kab_kota')->get();

        return response()->json(['data' => $getCity]);
    }

    public function getSchool()
    {
        $getSchool = School::select('id','sekolah')->where('kode_kab_kota',request()->get('city_code'))->orderBy('kode_kab_kota')->get();

        return response()->json(['data' => $getSchool]);
    }

    public function showJob($id,$student_id)
    {
        $job = JobVacancy::with('company')->findOrFail($id);
        $check = Apply::where('student_id',$student_id)->first();
        
        return response()->json(['message' => 'success', 'data' => $job, 'check' => $check]);
    }
}
