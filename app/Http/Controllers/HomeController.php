<?php

namespace App\Http\Controllers;

use App\Company;
use App\JobVacancy;
use App\Student;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $listadminsekolah = [];
        $id = auth()->user()->id;

        if (auth()->user()->level == 0) {
            $perusahaan = Company::count();
            $alumni = Student::count();
            $lowongan = JobVacancy::count();
            $listadminsekolah = User::where('level',1)->where('status',0)->paginate(10);
        }elseif(auth()->user()->level == 1)
        {
            $idsekolah = auth()->user()->school_id;
            $perusahaan = Company::where('user_id',$id)->count();
            $alumni = Student::where('school_id',$idsekolah)->count();
            $lowongan = JobVacancy::where('user_id',$id)->count();
        }
        return view('admin.home',compact('perusahaan','alumni','lowongan','listadminsekolah'));
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function profileUpdate(Request $request, $id)
    {
        $admin = User::findOrFail($id);
        $this->validate($request,[
            'name'          => 'required|string',
            'email'         => 'required|email|unique:users,email,' . $id,
            'phonenumber'   => 'required|numeric|min:6',
            'password'      => 'nullable|min:8|string',
        ]);

        $password = $request->password != '' ? bcrypt($request->password) : $admin->password;

        $admin->update([
            'name'          => $request->name,
            'email'         => $request->email,
            'phonenumber'   => $request->phonenumber,
            'password'      => $password,
        ]);

        return back()->with('message','Berhasil memperbaharui data');
    }
}
