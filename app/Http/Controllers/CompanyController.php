<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\User;

class CompanyController extends Controller
{
    public function index()
    {
        if (auth()->user()->level == 1) {
            $id = auth()->user()->id;
            $companies = Company::where('user_id',$id)->orderBy('name','ASC')->paginate(10);
        }else{
            $companies = Company::orderBy('name','ASC')->paginate(10);
        }
        return view('company.index',compact('companies'));
    }

    public function create()
    {
        $sekolah = User::with('sekolah')->where('level',1)->get();

        // dd($sekolah);
        return view('company.create',compact('sekolah'));
    }

    public function store(Request $request)
    {
        $id = '';
        if ($request->exists('user_id')) {
            $id = $request->user_id;
        }else{
            $id = auth()->user()->id;
        }
        $this->validate($request, [
            'name'          => 'required|string',
            'address'       => 'required|string',
            'description'   => 'required|string'
        ]);

        Company::create([
            'name'          => $request->name,
            'address'       => $request->address,
            'description'   => $request->description,
            'user_id'       => $id
        ]);

        return back()->with('message','Berhasil menambahkan data');
    }

    public function edit($id)
    {
        $sekolah = User::with('sekolah')->where('level',1)->get();
        $company = Company::findOrFail($id);

        return view('company.edit',compact('company','sekolah'));
    }

    public function update(Request $request, $id)
    {
        $company = Company::findOrFail($id);
        $user_id = '';
        if ($request->exists('user_id')) {
            $user_id = $request->user_id;
        }else{
            $user_id = auth()->user()->id;
        }

        $this->validate($request, [
            'name'          => 'required|string',
            'address'       => 'required|string',
            'description'   => 'required|string'
        ]);

        $company->update([
            'name'          => $request->name,
            'address'       => $request->address,
            'description'   => $request->description,
            'user_id'       => $user_id
        ]);

        return back()->with('message','Berhasil merubah data');
    }

    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        $company->delete();
    }
}
