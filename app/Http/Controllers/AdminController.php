<?php

namespace App\Http\Controllers;

use App\School;
use Illuminate\Http\Request;
use App\User;
use Response;

class AdminController extends Controller
{
    public function index()
    {
        $admin = User::with('sekolah')->where('level',1)->orderBy('name')->paginate(10);

        // dd($admin);

        return view('adminsekolah.index',compact('admin'));
    }

    public function create()
    {
        $school = School::get();
        return view('adminsekolah.create',compact('school'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required|string',
            'email'         => 'required|email|unique:users',
            'phonenumber'   => 'required|numeric|min:6',
            'password'      => 'required|min:8|string',
            'school_id'     => 'required|numeric|unique:users'
        ]);

        User::create([
            'name'          => $request->name,
            'email'         => $request->email,
            'phonenumber'   => $request->phonenumber,
            'password'      => bcrypt($request->password),
            'level'         => 1,
            'school_id'     => $request->school_id
        ]);

        return back()->with('message','Berhasil menambahkan data');
    }

    public function edit($id)
    {
        $admin = User::findOrFail($id);
        $school = School::get();
        return view('adminsekolah.edit',compact('school','admin'));
    }

    public function update(Request $request, $id)
    {
        $admin = User::findOrFail($id);
        $this->validate($request,[
            'name'          => 'required|string',
            'email'         => 'required|email|unique:users,email,' . $id,
            'phonenumber'   => 'required|numeric|min:6',
            'password'      => 'nullable|min:8|string',
            'school_id'     => 'required|numeric|unique:users,school_id,' . $id
        ]);

        $password = $request->password != '' ? bcrypt($request->password) : $admin->password;

        $admin->update([
            'name'          => $request->name,
            'email'         => $request->email,
            'phonenumber'   => $request->phonenumber,
            'password'      => bcrypt($request->password),
            'school_id'     => $request->school_id
        ]);

        return back()->with('message','Berhasil memperbaharui data');
    }

    public function destroy($id)
    {
        $admin = User::findOrFail($id);

        $admin->delete();
    }

    public function acc($id)
    {
      $admin = User::findOrFail($id);
      $admin->update([
        'status' => 1
      ]);
      return back()->with('message','Berhasil diverifikasi');
    }

    public function reject($id)
    {
      $admin = User::findOrFail($id);
      $admin->update([
        'status' => 2
      ]);
      return back()->with('message','Berhasil ditolak');
    }

    public function bukti($id)
    {
      $admin = User::findOrFail($id);
      $file= public_path(). "/storage/bukti/".$admin->bukti;

      $headers = array(
              'Content-Type: application/pdf',
            );

      return Response::download($file, 'filename.pdf', $headers);
    }
}
