<?php

use Illuminate\Support\Facades\Route;
use Whoops\Run;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FrontController@index');
Route::get('/daftar-sekolah','FrontController@daftarSekolah')->name('daftarsekolah');
Route::post('/daftar-sekolah','FrontController@storeDaftarSekolah')->name('daftarsekolah.store');
Route::get('/jobseeker/login','Jobseeker\LoginController@index')->name('login');
Route::post('/login/process','Jobseeker\LoginController@process')->name('login.process');
Route::post('/register','Jobseeker\RegisterController@register')->name('register');

Route::group(['prefix' => 'jobseeker', 'middleware' => 'students'], function(){
    Route::get('/jobvacancy','FrontController@jobVacancy')->name('jobvacancy');
    Route::get('/apply/{id}','FrontController@apply')->name('apply');
    Route::get('/informasi-lamaran','FrontController@informationApply')->name('informasi');
    Route::get('/profile','FrontController@profile')->name('profile.student');
    Route::post('/profile/{id}','FrontController@profileUpdate')->name('jobseeker.edit');
    Route::post('/logout','Jobseeker\LoginController@logout')->name('logout');
    Route::get('/postingan','FrontController@postingan')->name('postingan.index');
});

Route::group(['prefix' => 'admin'], function(){
    Route::get('/login','Auth\LoginController@loginAdmin')->name('login.admin');
    Route::post('/login','Auth\LoginController@loginProcessAdmin')->name('loginprocess.admin');
    Route::group(['middleware' => 'auth'], function(){
        Route::get('/dashboard','HomeController@index')->name('home.admin');
        Route::resource('company','CompanyController')->except(['show']);
        Route::resource('setting','SettingController')->except(['show']);
        Route::resource('alumni','StudentController')->except(['show']);
        Route::resource('admin','AdminController');
        Route::get('/adminsekolah/acc/{id}','AdminController@acc')->name('admin.acc');
        Route::get('/adminsekolah/reject/{id}','AdminController@reject')->name('admin.reject');
        Route::get('/adminsekolah/bukti/{id}','AdminController@bukti')->name('admin.bukti');
        Route::resource('bursakerja','JobVacancyController')->except(['show']);
        Route::get('/pelamar/{id}','JobVacancyController@pelamar')->name('pelamar');
        Route::post('/logout','Auth\LoginController@logout')->name('logout.admin');
        Route::get('/profile','HomeController@profile')->name('profile');
        Route::put('/profile/{id}','HomeController@profileUpdate')->name('profile.update');
        Route::get('/apply/accepted/{id}','JobVacancyController@accepted');
        Route::get('/apply/rejected/{id}','JobVacancyController@rejected');
        Route::post('/post','JobVacancyController@postStore')->name('post.store');
        Route::get('/apply/test/accpeted/{id}','JobVacancyController@testAccepted')->name('test.accepted');
        Route::get('/apply/test/rejected/{id}','JobVacancyController@testRejected')->name('test.rejected');
        Route::get('/apply/wawancara/accpeted/{id}','JobVacancyController@wawancaraAccepted')->name('wawancara.accepted');
        Route::get('/apply/wawancara/rejected/{id}','JobVacancyController@wawancaraRejected')->name('wawancara.rejected');
    } );
});
