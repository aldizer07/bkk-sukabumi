-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 03, 2021 at 03:57 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bkksmk`
--

-- --------------------------------------------------------

--
-- Table structure for table `applies`
--

CREATE TABLE `applies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_vacancy_id` bigint(20) UNSIGNED NOT NULL,
  `student_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `address`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(7, 'PT A', 'JL', 'Loker 2021', 8, '2021-09-02 18:56:45', '2021-09-02 18:56:45');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `job_vacancies`
--

CREATE TABLE `job_vacancies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` date NOT NULL,
  `start_salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `school_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_19_125025_create_companies_table', 1),
(5, '2020_12_20_102703_create_settings_table', 1),
(6, '2020_12_20_102902_create_students_table', 1),
(7, '2020_12_23_034303_create_student_education_table', 1),
(9, '2020_12_24_153817_create_job_vacancies_table', 2),
(10, '2020_12_27_113645_create_applies_table', 3),
(11, '2021_01_10_085013_create_posts_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `kode_prop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `propinsi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_kab_kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kabupaten_kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_kec` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `kecamatan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `npsn` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sekolah` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bentuk` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `alamat_jalan` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lintang` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `bujur` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `kode_prop`, `propinsi`, `kode_kab_kota`, `kabupaten_kota`, `kode_kec`, `kecamatan`, `npsn`, `sekolah`, `bentuk`, `status`, `alamat_jalan`, `lintang`, `bujur`) VALUES
(319, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '69757213', 'SMKS KESEHATAN TUNAS MADANI', 'SMK', 'S', 'JL. PELABUHAN II KM 8 TEGALLEGA', '-6.9656000', '106.8697000'),
(3029, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '20221564', 'SMKS KARTIKA III 2 SUKABUMI', 'SMK', 'S', 'JL. GOTONG ROYONG NO.64 SUKABUMI', '-6.9452000', '106.9517000'),
(3420, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20270889', 'SMKS PASIM PLUS SUKABUMI', 'SMK', 'S', 'JL. PRANA NO 8 RT 01/05', '-6.9190000', '106.9239000'),
(4795, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '20280579', 'SMKS PLUS AN-NABA', 'SMK', 'S', 'JL. WIDYAKRAMA NO. 112', '-6.9462000', '106.9227000'),
(5561, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '69754839', 'SMKS IT MADANI', 'SMK', 'S', 'JL. RAYA BAROS 01/07', '-6.9661000', '106.9443000'),
(8578, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '69757154', 'SMKS GEMA ISTIQOMAH', 'SMK', 'S', 'JL. WIDYAKRAMA BALANDONGAN NO. 1', '-6.9490000', '106.9117000'),
(9140, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '20221583', 'SLB A BUDI NURANI', 'SLB', 'S', 'Jalan Lio Balandongan Komplek Provelat No. 169', '-6.9485000', '106.9226000'),
(9531, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '69757214', 'SMKS PERSADA', 'SMK', 'S', 'JL KH MUSTOPA NO 24 SUBANG JAYA WETAN', '-6.9330000', '106.9356000'),
(9882, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '20253909', 'SMKS ULUL ALBAB SUKABUMI', 'SMK', 'S', 'JALAN KARAMAT KARANG TENGAH NO. 125', '-6.9010000', '106.9142000'),
(11424, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026203  ', 'Kec. Warudoyong', '20221571', 'SMKS PASUNDAN 2 SUKABUMI', 'SMK', 'S', 'JL. PASUNDAN NO.117 SUKABUMI', '-6.9237000', '106.9232000'),
(12559, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20221570', 'SMKN 3 SUKABUMI', 'SMK', 'N', 'JL. KABANDUNGAN NO.86 SUKABUMI', '-6.9050000', '106.9304000'),
(14347, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026203  ', 'Kec. Warudoyong', '20221573', 'SMKS ISLAM PENGUJI SUKABUMI', 'SMK', 'S', 'JL. KH. AHMAD SANUSI 195 SUKABUMI', '-6.9332000', '106.8913000'),
(14893, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026202  ', 'Kec. Citamiang', '20221574', 'SMKS PGRI 1 SUKABUMI', 'SMK', 'S', 'JL. PELABUHAN II CIPOHO INDAH SUKABUMI', '-6.9355000', '106.9267000'),
(14948, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026203  ', 'Kec. Warudoyong', '20229796', 'SMKS PASUNDAN 1 SUKABUMI', 'SMK', 'S', 'JL. PASUNDAN NO.117 SUKABUMI', '-6.9159000', '106.9055000'),
(15411, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '20221595', 'SMKS PLUS BINA TEKNIK YLPI SUKABUMI', 'SMK', 'S', 'JL. PELABUHAN II KM 7 SUKABUMI', '-6.9680000', '106.9026000'),
(16902, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026202  ', 'Kec. Citamiang', '69894099', 'SMK MUTIARA CENDEKIA', 'SMK', 'S', 'Jl. Lio Balandongan Sirnagalih No. 74', '-6.9455000', '106.9225000'),
(17436, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '20253907', 'SMKN 4 SUKABUMI', 'SMK', 'N', 'JALAN MERDEKA KM 4', '-6.9615000', '106.8952000'),
(17737, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20221568', 'SMKN 1 SUKABUMI', 'SMK', 'N', 'JL. KABANDUNGAN NO. 90', '-6.9057000', '106.9306000'),
(19309, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '20221618', 'SMKS YASPI SYAMSUL ULUM SUKABUMI', 'SMK', 'S', 'JL. BHAYANGKARA NO. 33', '-6.9169000', '106.9170000'),
(19941, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '20265663', 'SMKS TERPADU IBADURRAHMAN', 'SMK', 'S', 'JALAN PELABUHAN II KM. 7/JL KH ACUN MANSHUR', '-6.9611000', '106.9085000'),
(21642, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '20221585', 'SLB C Budi Nurani', 'SLB', 'S', 'Jl. Lio Balandongan Propelat', '-6.9474000', '106.9233000'),
(23346, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '20221616', 'SMKS SILIWANGI SUKABUMI', 'SMK', 'S', 'JL. TITIRAN BHAYANGKARA NO. 39 SUKABUMI', '-6.9041000', '106.9177000'),
(23689, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '69921197', 'SMK IT AL-FATH', 'SMK', 'S', 'Jl. Otto Iskandardinata No. 23B', '-6.9151454', '106.9454426'),
(24165, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20221565', 'SMKS KRISTEN BPK PENABUR SUKABUMI', 'SMK', 'S', 'JL. R. SYAMSUDIN S.H., NO. 60 SUKABUMI', '-6.9185000', '106.9357000'),
(24567, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '69899673', 'SMK IT AMAL ISLAMI', 'SMK', 'S', 'Jl. Sindangsari Lemburpasir No. 100 RT. 001/005', '-6.9530000', '106.9201000'),
(25050, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20221566', 'SMKS MUHAMMADIYAH 1 SUKABUMI', 'SMK', 'S', 'JL. SYAMSUDIN SH NO.59 SUKABUMI', '-6.9180000', '106.9352000'),
(25110, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026202  ', 'Kec. Citamiang', '20221572', 'SMKS PELITA YNH SUKABUMI', 'SMK', 'S', 'JL. OTISTA GG. PELITA SUKABUMI', '-6.9237000', '106.9232000'),
(25389, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026201  ', 'Kec. Baros', '20221584', 'SLB B BUDI NURANI', 'SLB', 'S', 'Jalan Lio Balandongan No.12', '-6.9485000', '106.9226000'),
(25663, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '69757153', 'SMKS PRIORITY', 'SMK', 'S', 'JL VETERAN I NO 25 TLP. (0266) 6249831', '-6.9247000', '106.9082000'),
(25976, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026202  ', 'Kec. Citamiang', '20221569', 'SMKN 2 SUKABUMI', 'SMK', 'N', 'JL. PELABUHAN II CIPOHO SUKABUMI', '-6.9349000', '106.9264000'),
(25978, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026202  ', 'Kec. Citamiang', '20221594', 'SMKS PGRI 2 SUKABUMI', 'SMK', 'S', 'JL. PELABUHAN II CIPOHO INDAH SUKABUMI', '-6.9270000', '106.9355000'),
(25985, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026205  ', 'Kec. Cikole', '20221617', 'SMKS TAMANSISWA SUKABUMI', 'SMK', 'S', 'R.SYAMSUDIN,SH NO. 58', '-6.9331000', '106.9298000'),
(26206, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026204  ', 'Kec. Gunung Puyuh', '20254980', 'SMKS KOMPUTER ABDI BANGSA SUKABUMI', 'SMK', 'S', 'Jalan Benteng Kidul Rt 01 Rw 01 Kec. Warudoyong', '-6.9265000', '106.9159000'),
(26838, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026206  ', 'Kec. Lembur Situ', '69757152', 'SMKS BINA SATYA MANDIRI', 'SMK', 'S', 'JL JERUK NYELAP NO 07', '-6.9745000', '106.8794000'),
(27641, '020000  ', 'Prov. Jawa Barat', '026200  ', 'Kota Sukabumi', '026203  ', 'Kec. Warudoyong', '20221596', 'SMKS TEKNOLOGI PLUS PADJADJARAN SUKABUMI', 'SMK', 'S', 'JL. CEMERLANG  NO.18', '-6.9166000', '106.9034000');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'Nama Sekolah', 'SMK Anak Bangsa', '2020-12-23 03:26:54', '2020-12-23 03:26:54'),
(2, 'Logo Sekolah', 'img/logo.png', '2020-12-23 03:28:22', '2020-12-23 03:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nisn` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school_id` bigint(20) NOT NULL,
  `cv` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `student_education`
--

CREATE TABLE `student_education` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `graduation_year` year(4) NOT NULL,
  `ipk` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `bukti` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phonenumber`, `email_verified_at`, `password`, `level`, `school_id`, `remember_token`, `created_at`, `updated_at`, `status`, `bukti`) VALUES
(2, 'Carleton Crooks', 'aidan.ullrich@example.com', NULL, '2020-12-22 22:14:44', '$2y$10$F7lSDEaPGI0oqpPSQbuZaem52QP.JSm4NwOVC0kB4KosoTQLvwOue', 0, 0, 'Ctewk830MhjsxnqkdbkxVGD1HeBKvQXQZml6HaGkYQp3mX4fz0mEeRSTH1Pr', '2020-12-22 22:14:44', '2020-12-22 22:14:44', 1, NULL),
(8, 'Admin BKK Smk Mutiara Cendikia', 'bkksmkcendikia@gmail.com', '07482747', NULL, '$2y$10$.HarG4lbYMmFYZ4hXZCqB.ZL8zoZTKfsNYgyKFiqpX3GacBpUdrtG', 1, 16902, NULL, '2021-09-02 18:32:25', '2021-09-02 18:56:10', 1, '335915971.pdf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applies`
--
ALTER TABLE `applies`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `job_vacancies`
--
ALTER TABLE `job_vacancies`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `students_nisn_unique` (`nisn`) USING BTREE,
  ADD UNIQUE KEY `students_email_unique` (`email`) USING BTREE,
  ADD KEY `school_id` (`school_id`) USING BTREE;

--
-- Indexes for table `student_education`
--
ALTER TABLE `student_education`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applies`
--
ALTER TABLE `applies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_vacancies`
--
ALTER TABLE `job_vacancies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27642;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_education`
--
ALTER TABLE `student_education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
